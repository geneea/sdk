.. _sdk:

Geneea SDK
##########

We provide an SDK for the General API in Python. It is available via ``pip`` from `pypi <https://pypi.org/project/geneea-nlp-client/>`__.

See

* :ref:`Quick start guide<quick-python-sdk>` for a simple example,
* :ref:`API Reference<sdk_api>`.

The code is publicly available at `bitbucket <https://bitbucket.org/geneea/sdk/>`__ , it also contains more
`examples <https://bitbucket.org/geneea/sdk/src/master/examples/g3/>`__ in the examples directory.


.. toctree::
    :hidden:
    :maxdepth: 2

    geneeanlpclient
