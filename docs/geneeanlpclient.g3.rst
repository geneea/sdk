.. _sdk_api_g3:

geneeanlpclient.g3 package
==========================

SDK encapsulating Geneea General API (G3) - see :ref:`G3 API documentation<api_general>`.

The SDK has the following main parts:

* :ref:`Client <sdk_api_g3_client>` – a simple REST client
* :ref:`Request <sdk_api_g3_request>` – an object encapsulating the request the Client send to the G3 API; typically it is built via RequestBuilder
* :ref:`G3 <sdk_api_g3_response>` – object encapsulating the response of the API
* :ref:`Readers/Writers <sdk_api_g3_rw>` – objects for reading/writing from/to json



.. _sdk_api_g3_client:

The client
----------

.. automodule:: geneeanlpclient.g3.client
    :members:
    :undoc-members:
    :show-inheritance:


.. _sdk_api_g3_request:

The Request Objects
-------------------

.. automodule:: geneeanlpclient.g3.request
    :members:
    :undoc-members:
    :show-inheritance:


.. _sdk_api_g3_response:

The Response objects
--------------------

.. automodule:: geneeanlpclient.g3.model
    :members:
    :undoc-members:
    :show-inheritance:


.. _sdk_api_g3_rw:

Reading/Writing from/to json
----------------------------

.. automodule:: geneeanlpclient.g3.reader
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: geneeanlpclient.g3.writer
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: geneeanlpclient.g3.f2converter
    :members:
    :undoc-members:
    :show-inheritance:
