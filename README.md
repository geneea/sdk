# Geneea Interpretor Client

Interpretor is a Natural Language Processing platform which helps the users leverage their text data. Features include 

- language detection
- entity recognition
- semantic tagging
- sentiment analysis

This package contains Python SDK which enables easy integration of Interpretor [General NLP REST API](https://help.geneea.com/api_general/index.html#api-general) with your code.

For detailed information, please [read the docs](https://help.geneea.com/sdk/index.html).
