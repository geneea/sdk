#!/usr/bin/env bash

# uncomment to generate the default documentation (it needs heavy post-editing to be reasonably well structured)
# edit and then move to docs
#
# rm -r docs/generated_docs
# sphinx-apidoc -o docs/generated_docs  . test* examples* setup*

rm -r build/built_docs
sphinx-build -b html -c docs docs build/built_docs
sphinx-build -b html -c docs docs build/built_docs